#pragma once
#include "Mwette.h"


class Environnement
{
	///////////// Enumerations
public:
	typedef enum { None, Dirt, Water, Fish, Nest } GroundState;


	///////////// Attributs (with default values)
protected:
	int width = 20;
	int height = 20;
	GroundState** env = NULL;
	int nbWaterSpawnPoints = 3;
	int WaterRadius = 2;
	int nbNest = 5;
	int nbFish = 5;
	std::vector<Mwette> mwetteList = std::vector<Mwette>();

	bool displayDebug = false;

protected:
	void generateTerrain();
	void generateStructure(GroundState in_GState);
	void generateStructures();

	///////////// Constructeurs/Destructeurs
public:
	Environnement(void);
	Environnement(int height, int width);
	~Environnement(void);
	
	//////////////  Accesseurs
public:
	void init(bool displayDebug);
	int getWidth();
	int getHeight();
	GroundState getCaseState(int h, int w);
	void setCaseState(int h, int w, GroundState gState);
	std::string toString();
	void gameLoop();
	void percept(Mwette& mAgent);
	void addMwette(Mwette mAgent);
};