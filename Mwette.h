#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string>
#include <iostream>
#include <vector>
#include <unistd.h>



class Mwette
{
	///////////// Enumerations
public:
	typedef enum { Idle, Walking, Swimming, Resting, Fishing, Dead } MwetteState;
	typedef enum { None, Eat, Rest, Patrol } MwetteDesire;
	


	///////////// Attributs (with default values)
protected:
	int posX = 0;
	int posY = 0;
	int blockedDir = 0;
	//Static ---------
	int maxPosX = 20;
	int maxPosY = 20;
	//--------------
	int saturation = 100;
	int energy = 100;
	int viewRadius = 5;

	int walkCost = 1;
	int swimCost = 2;
	int fishingCost = 2;

	MwetteState activeState = Idle;
	MwetteDesire activeDesire = None;

	int seuilCritique = 30;
	int seuilAcceptable = 80;

	int nearestNestPosX = -1;
	int nearestNestPosY = -1;

	int nearestFishPosX = -1;
	int nearestFishPosY = -1;

	int nearestGroundPatchPosX = -1;
	int nearestGroundPatchPosY = -1;

	int nearestWaterPatchPosX = -1;
	int nearestWaterPatchPosY = -1;

	//Communication Mwette mwette ---------

	std::vector<Mwette> mwetteInView = std::vector<Mwette>();

	//Used for communication
	int lastSeenNestPosX = -1;
	int lastSeenNestPosY = -1;

	//Used for communication
	int lastSeenFishPosX = -1;
	int lastSeenFishPosY = -1;


	bool displayDebug = false;


	///////////// Constructeurs/Destructeurs
public:
	Mwette(bool displayDebug = false);
	Mwette(int posY, int posX, bool displayDebug = false);
	Mwette(int posY, int posX, int statingSaturation, int startingEnergy, bool displayDebug = false);
	~Mwette(void);
	
	//////////////  Accesseurs
public:
	MwetteState getState();
	void setState(MwetteState in_mState);
	void deliberate();
	void plan();
	void randomWalk();
	void action(int in_targetPosY, int in_targetPosX);
	int getPosX();
	int getPosY();
	void setPos(int in_posY, int in_posX);

	int getBlockedDir();
	void setBlockedDir(int in_iBlockedDir);

	int getEnergy();
	void setEnergy(int in_iEnergy);

	int getWalkCost();
	int getSwimCost();
	int getFishingCost();

	int getSaturation();
	void setSaturation(int in_iSaturation);

	int getSeuilCritique();
	int getSeuilAcceptable();

	MwetteDesire getDesire();
	void setDesire(MwetteDesire in_mDesire);

	int getNearestNestPosX();
	int getNearestNestPosY();
	int getNearestFishPosX();
	int getNearestFishPosY();
	int getNearestGroundPosX();
	int getNearestGroundPosY();
	int getNearestWaterPosX();
	int getNearestWaterPosY();

	int getLastSeenNestPosX();
	int getLastSeenNestPosY();
	int getLastSeenFishPosX();
	int getLastSeenFishPosY();

	Mwette getMwetteInViewByIndex(int in_iPos);
	unsigned int getNbMwetteInView();
	void setMwettesInView(std::vector<Mwette> in_vecMwetteInView);


	int getViewRadius();

	void setNestInView(int in_nestPosY, int in_nestPosX);
	void setFishInView(int in_fishPosY, int in_fishPosX);
	void setGroundInView(int in_groundPosY, int in_groundPosX);
	void setWaterInView(int in_waterPosY, int in_waterPosX);

	void setLastSeenNest(int in_lastSeenNestPosY, int in_lastSeenNestPosX);
	void setLastSeenFish(int in_lastSeenFishPosY, int in_lastSeenFishPosX);


	bool isNestInView();
	bool isFishInView();
	bool isGroundInView();
	bool isWaterInView();
	bool knowsNestSpot();
	bool knowsFishSpot();
};