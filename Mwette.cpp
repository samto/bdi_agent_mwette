#include "Mwette.h"

Mwette::Mwette(bool displayDebug){
	this->displayDebug = displayDebug;
}

Mwette::Mwette(int posY, int posX, bool displayDebug){
	this->posX = posX;
	this->posY = posY;
	this->displayDebug = displayDebug;
}

Mwette::Mwette(int posY, int posX, int startingSaturation, int startingEnergy, bool displayDebug){
	this->posX = posX;
	this->posY = posY;
	this->saturation = startingSaturation;
	this->energy = startingEnergy;
	this->displayDebug = displayDebug;

}

Mwette::~Mwette(){

}

void Mwette::setState(Mwette::MwetteState in_mState){
	this->activeState = in_mState;
}

Mwette::MwetteState Mwette::getState(){
	return activeState;
}

void Mwette::deliberate(){
	if(displayDebug){
		std::cout<<"Nearest Nest > "<<getNearestNestPosY()<<" "<<getNearestNestPosX()<<std::endl;
		std::cout<<"Nearest Fish > "<<getNearestFishPosY()<<" "<<getNearestFishPosX()<<std::endl;
		std::cout<<"Nearest Ground > "<<getNearestGroundPosY()<<" "<<getNearestGroundPosX()<<std::endl;
		std::cout<<"Nearest Water > "<<getNearestWaterPosY()<<" "<<getNearestWaterPosX()<<std::endl;
		std::cout<<"LastSeen Nest > "<<getLastSeenNestPosY()<<" "<<getLastSeenNestPosX()<<std::endl;
		std::cout<<"LastSeen Fish > "<<getLastSeenFishPosY()<<" "<<getLastSeenFishPosX()<<std::endl;
		std::cout<<"------------------------------------------------------------"<<std::endl;
	}
	if(this->getEnergy() < this->getSeuilCritique() || this->getEnergy() < this->getSaturation() || (this->getSaturation() > this->getSeuilAcceptable() && this->isNestInView())){
		this->setDesire(Rest);
	} else if(this->getSaturation() < this->getSeuilCritique() || this->getSaturation() < this->getEnergy() || (this->getEnergy() > this->getSeuilAcceptable() && this->isFishInView())){
		this->setDesire(Eat);
	} else {
		this->setDesire(Patrol);
	}
}

void Mwette::plan(){
	srand(time(NULL));
	if(this->getDesire() == Mwette::Rest){
		if(!this->knowsNestSpot()){
			unsigned int cptMwette = 0;
			while(cptMwette < this->getNbMwetteInView() && !this->knowsNestSpot()){
				if(this->getMwetteInViewByIndex(cptMwette).knowsNestSpot()){
					std::cout<<"##### Communication Mwette Mwette effectuée - Nid #####"<<std::endl;
					this->setLastSeenNest(this->getMwetteInViewByIndex(cptMwette).getLastSeenNestPosY(), this->getMwetteInViewByIndex(cptMwette).getLastSeenNestPosX());
				}
				cptMwette++;
			}
		}
		if(this->isNestInView()){
			//If the mwette is on the nest then rest
			if(this->getPosX() == this->getNearestNestPosX() && this->getPosY() == this->getNearestNestPosY()){
				this->setState(Mwette::Resting);
				this->action(this->getPosY(), this->getPosX());
			} else if(this->getPosX() != this->getNearestNestPosX()){
				int step = this->getPosX() < this->getNearestNestPosX() ? 1 : -1;
				this->action(this->getPosY(), this->getPosX() + step);
			} else if(this->getPosY() != this->getNearestNestPosY()){
				int step = this->getPosY() < this->getNearestNestPosY() ? 1 : -1;
				this->action(this->getPosY() + step, this->getPosX());
			}
		} else if (this->knowsNestSpot()){
			if(this->getPosX() != this->getLastSeenNestPosX()){
				int step = this->getPosX() < this->getLastSeenNestPosX() ? 1 : -1;
				this->action(this->getPosY(), this->getPosX() + step);
			} else if(this->getPosY() != this->getLastSeenNestPosY()){
				int step = this->getPosY() < this->getLastSeenNestPosY() ? 1 : -1;
				this->action(this->getPosY() + step, this->getPosX());
			}
		} else if (this->getState() == Mwette::Swimming && this->isGroundInView()){
			if(this->getPosX() != this->getNearestGroundPosX()){
				int step = this->getPosX() < this->getNearestGroundPosX() ? 1 : -1;
				this->action(this->getPosY(), this->getPosX() + step);
			} else if(this->getPosY() != this->getNearestGroundPosY()){
				int step = this->getPosY() < this->getNearestGroundPosY() ? 1 : -1;
				this->action(this->getPosY() + step, this->getPosX());
			}
		} else {
			randomWalk();
		}
	}
	else if (this->getDesire() == Mwette::Eat){
		if(!this->knowsFishSpot()){
			unsigned int cptMwette = 0;
			while(cptMwette < this->getNbMwetteInView() && !this->knowsFishSpot()){
				if(this->getMwetteInViewByIndex(cptMwette).knowsFishSpot()){
					std::cout<<"##### Communication Mwette Mwette effectuée - Poisson #####"<<std::endl;
					this->setLastSeenFish(this->getMwetteInViewByIndex(cptMwette).getLastSeenFishPosY(), this->getMwetteInViewByIndex(cptMwette).getLastSeenFishPosX());
				}
				cptMwette++;
			}
		}
		if(this->isFishInView()){
			//If the mwette is on the fishing spot then eat
			if(this->getPosX() == this->getNearestFishPosX() && this->getPosY() == this->getNearestFishPosY()){
				this->setState(Mwette::Fishing);
				this->action(this->getPosY(), this->getPosX());
			} else if(this->getPosX() != this->getNearestFishPosX()){
				int step = this->getPosX() < this->getNearestFishPosX() ? 1 : -1;
				this->action(this->getPosY(), this->getPosX() + step);
			} else if(this->getPosY() != this->getNearestFishPosY()){
				int step = this->getPosY() < this->getNearestFishPosY() ? 1 : -1;
				this->action(this->getPosY() + step, this->getPosX());
			}
		} else if (this->knowsFishSpot()){
			if(this->getPosX() != this->getLastSeenFishPosX()){
				int step = this->getPosX() < this->getLastSeenFishPosX() ? 1 : -1;
				this->action(this->getPosY(), this->getPosX() + step);
			} else if(this->getPosY() != this->getLastSeenFishPosY()){
				int step = this->getPosY() < this->getLastSeenFishPosY() ? 1 : -1;
				this->action(this->getPosY() + step, this->getPosX());
			}
		} else if (this->getState() == Mwette::Walking && this->isWaterInView()){
			if(this->getPosX() != this->getNearestWaterPosX()){
				int step = this->getPosX() < this->getNearestWaterPosX() ? 1 : -1;
				this->action(this->getPosY(), this->getPosX() + step);
			} else if(this->getPosY() != this->getNearestWaterPosY()){
				int step = this->getPosY() < this->getNearestWaterPosY() ? 1 : -1;
				this->action(this->getPosY() + step, this->getPosX());
			}
		} else {
			randomWalk();
		}
	} else if(this->getDesire() == Mwette::Patrol){
		randomWalk();
	}
}

void Mwette::randomWalk(){
	bool actionDone = false;
			do{
				int randDir = rand()%4+1;
				switch(randDir){
					//South
					case 1:
						if(this->getPosY() < maxPosY-1 && this->getBlockedDir() != 1){
							this->action(this->getPosY() + 1, this->getPosX());
							this->setBlockedDir(2);
							actionDone = true;
						}
						break;
					//North
					case 2:
						if(this->getPosY() > 0 && this->getBlockedDir() != 2){
							this->action(this->getPosY() -1, this->getPosX());
							this->setBlockedDir(1);
							actionDone = true;
						}
						break;
					//East
					case 3:
						if(this->getPosX() < maxPosX - 1 && this->getBlockedDir() != 3){
							this->action(this->getPosY(), this->getPosX() + 1);
							this->setBlockedDir(4);
							actionDone = true;
						}
						break;
					//West
					case 4:
						if(this->getPosX() > 0 && this->getBlockedDir() != 4){
							this->action(this->getPosY(), this->getPosX() - 1);
							this->setBlockedDir(3);
							actionDone = true;
						}
						break;

				}
			} while(!actionDone);
}

void Mwette::action(int in_targetPosY, int in_targetPosX){
	if(this->getState() == Walking){
		if(in_targetPosX - this->getPosX() == 1){
			this->setBlockedDir(4);
		} else if(in_targetPosX - this->getPosX() == -1) {
			this->setBlockedDir(3);
		} else if(in_targetPosY - this->getPosY() == 1) {
			this->setBlockedDir(2);
		} else if(in_targetPosY - this->getPosY() == -1) {
			this->setBlockedDir(1);
		}
		setPos(in_targetPosY, in_targetPosX);
		setEnergy(getEnergy()-getWalkCost());
	} else if (this->getState() == Swimming){
		if(in_targetPosX - this->getPosX() == 1){
			this->setBlockedDir(4);
		} else if(in_targetPosX - this->getPosX() == -1) {
			this->setBlockedDir(3);
		} else if(in_targetPosY - this->getPosY() == 1) {
			this->setBlockedDir(2);
		} else if(in_targetPosY - this->getPosY() == -1) {
			this->setBlockedDir(1);
		}
		setPos(in_targetPosY, in_targetPosX);
		setEnergy(getEnergy()-getSwimCost());
	}
	else if (this->getState() == Resting){
		setEnergy(100);
	} 
	else if (this->getState() == Fishing){
		setSaturation(100);
	}
	setSaturation(getSaturation()-1);

	//If the mwette validates the dying conditions then set its state to dead
	if(this->getEnergy() <= 0 || this->getSaturation() <= 0){
		this->setState(Dead);
	}
}

int Mwette::getPosX(){
	return this->posX;
}

int Mwette::getPosY(){
	return this->posY;
}

void Mwette::setPos(int in_posY, int in_posX){
	this->posX = in_posX;
	this->posY = in_posY;
}

int Mwette::getBlockedDir(){
	return this->blockedDir;
}

void Mwette::setBlockedDir(int in_iBlockedDir){
	this->blockedDir = in_iBlockedDir;
}

int Mwette::getEnergy(){
	return this->energy;
}


void Mwette::setEnergy(int in_iEnergy){
	this->energy = in_iEnergy;
}

int Mwette::getWalkCost(){
	return this->walkCost;
}

int Mwette::getSwimCost(){
	return this->swimCost;
}

int Mwette::getFishingCost(){
	return this->fishingCost;
}

int Mwette::getSaturation(){
	return this->saturation;
}

void Mwette::setSaturation(int in_iSaturation){
	this->saturation = in_iSaturation;
}

int Mwette::getSeuilCritique(){
	return this->seuilCritique;
}
	
int Mwette::getSeuilAcceptable(){
	return this->seuilAcceptable;
}

Mwette::MwetteDesire Mwette::getDesire(){
	return this->activeDesire;
}

void Mwette::setDesire(Mwette::MwetteDesire in_mDesire){
	this->activeDesire = in_mDesire;
}

int Mwette::getNearestNestPosX(){
	return this->nearestNestPosX;
}

int Mwette::getNearestNestPosY(){
	return this->nearestNestPosY;
}

int Mwette::getNearestFishPosX(){
	return this->nearestFishPosX;
}

int Mwette::getNearestFishPosY(){
	return this->nearestFishPosY;
}

int Mwette::getNearestGroundPosX(){
	return this->nearestGroundPatchPosX;
}

int Mwette::getNearestGroundPosY(){
	return this->nearestGroundPatchPosY;
}

int Mwette::getNearestWaterPosX(){
	return this->nearestWaterPatchPosX;
}

int Mwette::getNearestWaterPosY(){
	return this->nearestWaterPatchPosY;
}

int Mwette::getLastSeenNestPosX(){
	return this->lastSeenNestPosX;
}
int Mwette::getLastSeenNestPosY(){
	return this->lastSeenNestPosY;
}
int Mwette::getLastSeenFishPosX(){
	return this->lastSeenFishPosX;
}
int Mwette::getLastSeenFishPosY(){
	return this->lastSeenFishPosY;
}

Mwette Mwette::getMwetteInViewByIndex(int in_iPos){
	return this->mwetteInView[in_iPos];
}

unsigned int Mwette::getNbMwetteInView(){
	return this->mwetteInView.size();
}

void Mwette::setMwettesInView(std::vector<Mwette> in_vecMwetteInView){
	this->mwetteInView = in_vecMwetteInView;
}

int Mwette::getViewRadius(){
	return this->viewRadius;
}

void Mwette::setNestInView(int in_nestPosY, int in_nestPosX){
	this->nearestNestPosX = in_nestPosX;
	this->nearestNestPosY = in_nestPosY;

	if(isNestInView()){
		setLastSeenNest(in_nestPosY, in_nestPosX);
	}
}

void Mwette::setFishInView(int in_fishPosY, int in_fishPosX){
	this->nearestFishPosX = in_fishPosX;
	this->nearestFishPosY = in_fishPosY;

	if(isFishInView()){
		setLastSeenFish(in_fishPosY, in_fishPosX);
	}
}

void Mwette::setGroundInView(int in_groundPosY, int in_groundPosX){
	this->nearestGroundPatchPosX = in_groundPosX;
	this->nearestGroundPatchPosY = in_groundPosY;
}

void Mwette::setWaterInView(int in_waterPosY, int in_waterPosX){
	this->nearestWaterPatchPosX = in_waterPosX;
	this->nearestWaterPatchPosY = in_waterPosY;
}

void Mwette::setLastSeenNest(int in_lastSeenNestPosY, int in_lastSeenNestPosX){
	this->lastSeenNestPosX = in_lastSeenNestPosX;
	this->lastSeenNestPosY = in_lastSeenNestPosY;
}
void Mwette::setLastSeenFish(int in_lastSeenFishPosY, int in_lastSeenFishPosX){
	this->lastSeenFishPosX = in_lastSeenFishPosX;
	this->lastSeenFishPosY = in_lastSeenFishPosY;
}

bool Mwette::isNestInView(){
	return this->getNearestNestPosX() != -1 && this->getNearestNestPosY() != -1;
}

bool Mwette::isFishInView(){
	return this->getNearestFishPosX() != -1 && this->getNearestFishPosY() != -1;
}

bool Mwette::isGroundInView(){
	return this->getNearestGroundPosX() != -1 && this->getNearestGroundPosY() != -1;
}

bool Mwette::isWaterInView(){
	return this->getNearestWaterPosX() != -1 && this->getNearestWaterPosY() != -1;
}

bool Mwette::knowsNestSpot(){
	return this->getLastSeenNestPosX() != -1 && this->getLastSeenNestPosY() != -1;
}
bool Mwette::knowsFishSpot(){
	return this->getLastSeenFishPosX() != -1 && this->getLastSeenFishPosY() != -1;
}