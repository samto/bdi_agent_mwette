CXX=g++
CXX_FLAGS=-g -O0 -ansi -pedantic -w

LD_FLAGS =

PROGS=main

SOURCES=main.cpp Environnement.cpp Mwette.cpp

HEADERS=Environnement.h Mwette.h

DEPFILE=.depend

ifeq ($(wildcard $(DEPFILE)), )

all: $(DEPFILE)
	make

else

all: $(PROGS)
include $(DEPFILE)

endif

include $(DEPFILE)

.SUFFIXES: .cpp .h .o

$(PROGS): $(SOURCES:.cpp=.o)
	$(CXX) $^ -o $@ $(LD_FLAGS)

.cpp.o:
	$(CXX) $(CXX_FLAGS) $< -c

$(DEPFILE): $(SOURCES)
	$(CXX) -MM $^ > $@

clean:
	rm -f *~ $(SOURCES:.cpp=.o) $(PROGS)
