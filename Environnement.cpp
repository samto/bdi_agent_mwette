#include "Environnement.h"


Environnement::Environnement(void)
{
	srand (time(NULL));
}

Environnement::Environnement(int height, int width)
{
	srand (time(NULL));
	this->width = width;
	this->height = height;
}

Environnement::~Environnement(void)
{
}


void Environnement::init(bool displayDebug){

	env = new GroundState*[getHeight()];
	for(int h = 0; h < getHeight(); h++){
		env[h] = new GroundState[getWidth()];
	}

	this->displayDebug = displayDebug;

	this->generateTerrain();
	this->generateStructures();
}

void Environnement::generateTerrain(){

	//Fill land with dirt
	for(int h = 0; h < getHeight(); h++){
		for(int w = 0; w < getWidth(); w++){
			env[h][w] = Dirt;
		}
	}

	//Set some default spawnpoints for water (3 spawnpoints)
	int randHeight, randWidth;
	for(int cptSpawnPoint = 0; cptSpawnPoint < this->nbWaterSpawnPoints; cptSpawnPoint++){
		do {
			randWidth = rand() % getWidth();
			randHeight = rand() % getHeight();
		} while(getCaseState(randWidth,randHeight) == Water);

		env[randHeight][randWidth] = Water;

		//Propagate the water

		//With squares method ---------------
		for(int h = randHeight - WaterRadius; h <= randHeight + WaterRadius; h++){
			for(int w = randWidth - WaterRadius; w <= randWidth + WaterRadius; w++){
				setCaseState(h,w,Water);
			}
		}
	}

}

//Generates one structure at random
void Environnement::generateStructure(GroundState in_GState){
	srand(time(NULL));
	//Generate a random position to set a structure in
	int randHeight, randWidth;
	bool structureGenerated = false;

	GroundState groundStateToSwap = None;

	switch(in_GState){
		case Fish:
			groundStateToSwap = Water; 
			break;
		case Nest:
			groundStateToSwap = Dirt;
			break;
	}

	do {
		randWidth = rand() % getWidth();
		randHeight = rand() % getHeight();

		if(getCaseState(randHeight, randWidth) == groundStateToSwap){
			setCaseState(randHeight, randWidth, in_GState);
			structureGenerated = true;
		}
	} while(!structureGenerated);
}

void Environnement::generateStructures(){


	//Generate a random position to set a structure in
	int randHeight, randWidth;
	int remainingNest = this->nbNest, remainingFish = this->nbFish;
	while(remainingFish > 0 || remainingNest > 0) {
		randWidth = rand() % getWidth();
		randHeight = rand() % getHeight();

		if(getCaseState(randHeight, randWidth) == Water && remainingFish > 0){
			//Place a fish
			remainingFish--;
			setCaseState(randHeight, randWidth, Fish);
		} else if(getCaseState(randHeight, randWidth) == Dirt && remainingNest > 0){
			//Place a nest
			remainingNest--;
			setCaseState(randHeight, randWidth, Nest);
		}

	}
}

int Environnement::getWidth(){
	return this->width;
}

int Environnement::getHeight(){
	return this->height;
}

Environnement::GroundState Environnement::getCaseState(int h, int w){
	if(h >= 0 && h < this->getHeight() && w >= 0 && w < this->getWidth()){
		return env[h][w];
	} else {
		return None;
	}
}

void Environnement::setCaseState(int h, int w, GroundState gState){
	if(w < 0 || w >= getWidth() || h < 0 || h >= getHeight()){
		return;
	}

	env[h][w] = gState;
}

std::string Environnement::toString(){
	std::string terrainString = "";
	bool mwetteOnCase = false;

	for(int h = 0; h < getHeight(); h++){
		for(int w = 0; w < getWidth(); w++){
			mwetteOnCase = false;
			for(int mwetteIndex = 0; mwetteIndex < mwetteList.size(); mwetteIndex++){
				if(mwetteList[mwetteIndex].getPosY() == h && mwetteList[mwetteIndex].getPosX() == w){
					mwetteOnCase = true;
					terrainString += "\033[1;37m☺";
				}
			}
			if(!mwetteOnCase){
				switch(getCaseState(h,w)){
					case Dirt:
						terrainString += "\033[0;32m▓";
						break;
					case Water:
						terrainString += "\033[1;36m~";
						break;
					case Fish:
						terrainString += "\033[1;34mÞ";
						break;
					case Nest:
						terrainString += "\033[1;33mO";
						break;
				}
			}
		}
		terrainString += "\n";
	}

	return terrainString;
}

void Environnement::gameLoop(){
	int gameLoopIter = 0;
	while(mwetteList.size()>0){
		//Agents actions
		std::vector<int> deadMwetteIndex = std::vector<int>();
		for(int mwetteIndex = 0; mwetteIndex < mwetteList.size(); mwetteIndex++){
			percept(mwetteList[mwetteIndex]);

			mwetteList[mwetteIndex].deliberate();

			mwetteList[mwetteIndex].plan();

			//If the mwette died
			if(mwetteList[mwetteIndex].getState() == Mwette::Dead){
				//Remove the mwette from the list
				deadMwetteIndex.push_back(mwetteIndex);
			}
			//Environment actions
			else if(mwetteList[mwetteIndex].getState() == Mwette::Resting){
				generateStructure(Nest);
				setCaseState(mwetteList[mwetteIndex].getPosY(),mwetteList[mwetteIndex].getPosX(),Dirt);
				
			} 
			else if(mwetteList[mwetteIndex].getState() == Mwette::Fishing){
				generateStructure(Fish);
				setCaseState(mwetteList[mwetteIndex].getPosY(),mwetteList[mwetteIndex].getPosX(),Water);
				
			}

			//State based on the ground type
			if(getCaseState(mwetteList[mwetteIndex].getPosY(), mwetteList[mwetteIndex].getPosX()) == Dirt){
				mwetteList[mwetteIndex].setState(Mwette::Walking);
			} else if(getCaseState(mwetteList[mwetteIndex].getPosY(), mwetteList[mwetteIndex].getPosX()) == Water) {
				mwetteList[mwetteIndex].setState(Mwette::Swimming);
			}
			
			
		}


		for(int deadMwette = deadMwetteIndex.size()-1; deadMwette >= 0 ; deadMwette--){
			mwetteList.erase(mwetteList.begin()+deadMwetteIndex[deadMwette]); //Douteux ------- A verif
		}



		//Print environnement
		std::cout<<toString()<<std::endl;
		std::cout<<"["<<gameLoopIter<<"]--------------------------------------"<<std::endl;
		if(displayDebug){
			std::cout<<"Step "<<gameLoopIter<<" | Mwette Energy > "<<mwetteList[0].getEnergy()<<" | Mwette Saturation > "<<mwetteList[0].getSaturation()<<" | Desire : "<<mwetteList[0].getDesire()<<" | State : "<<mwetteList[0].getState()<<std::endl;
			std::cout<<"--------------------------------------"<<std::endl;
		}

		//Sleep until next step
		sleep(1);
		gameLoopIter++;
	}
}

void Environnement::addMwette(Mwette mAgent){
	mwetteList.push_back(mAgent);
}

void Environnement::percept(Mwette& mAgent){
	//Nest perception
	int nestPosX=-1, nestPosY=-1;
	int minDistNest = 2*mAgent.getViewRadius()+1;
	//Fish perception
	int fishPosX=-1, fishPosY=-1;
	int minDistFish = 2*mAgent.getViewRadius()+1;
	//Ground perception
	int groundPosX=-1, groundPosY=-1;
	int minDistGround = 2*mAgent.getViewRadius()+1;
	//Water perception
	int waterPosX=-1, waterPosY=-1;
	int minDistWater = 2*mAgent.getViewRadius()+1;
	//---------------------------
	if(displayDebug)
		std::cout<<"Agent view : \n-----------------------------------------"<<std::endl;
	for(int h = mAgent.getPosY() - mAgent.getViewRadius(); h < mAgent.getPosY() + mAgent.getViewRadius(); h++){
		for(int w = mAgent.getPosX() - mAgent.getViewRadius(); w < mAgent.getPosX() + mAgent.getViewRadius(); w++){
			if(getCaseState(h,w) == Nest && abs(mAgent.getPosY() - h) + abs(mAgent.getPosX() - w) < minDistNest){
				minDistNest = abs(mAgent.getPosY() - h) + abs(mAgent.getPosX() - w);
				nestPosX = w;
				nestPosY = h;
			} else if (getCaseState(h,w) == Fish && abs(mAgent.getPosY() - h) + abs(mAgent.getPosX() - w) < minDistFish) {
				minDistFish = abs(mAgent.getPosY() - h) + abs(mAgent.getPosX() - w);
				fishPosX = w;
				fishPosY = h;
			} else if (getCaseState(h,w) == Dirt && abs(mAgent.getPosY() - h) + abs(mAgent.getPosX() - w) < minDistGround) {
				minDistGround = abs(mAgent.getPosY() - h) + abs(mAgent.getPosX() - w);
				groundPosX = w;
				groundPosY = h;
			} else if (getCaseState(h,w) == Water && abs(mAgent.getPosY() - h) + abs(mAgent.getPosX() - w) < minDistWater) {
				minDistWater = abs(mAgent.getPosY() - h) + abs(mAgent.getPosX() - w);
				waterPosX = w;
				waterPosY = h;
			}
			if(displayDebug)
				std::cout<<getCaseState(h,w)<<" ";
		}
		if(displayDebug)
			std::cout<<std::endl;
	}
	if(displayDebug)
		std::cout<<"-----------------------------------------"<<std::endl;

	std::vector<Mwette> mwetteInView = std::vector<Mwette>();

	
	for(int mwetteIndex = 0; mwetteIndex < mwetteList.size(); mwetteIndex++){
		if(&mAgent != &mwetteList[mwetteIndex])
			if(abs(mAgent.getPosY() - mwetteList[mwetteIndex].getPosY()) < mAgent.getViewRadius() && abs(mAgent.getPosX() - mwetteList[mwetteIndex].getPosX()) < mAgent.getViewRadius()){
				mwetteInView.push_back(&mwetteList[mwetteIndex]);
			}
	}


	//If the last seen nest pos is in view but not a nest anymore then reset the lastSeen Nest
	if(abs(mAgent.getPosY() - mAgent.getLastSeenNestPosY()) < mAgent.getViewRadius() && abs(mAgent.getPosX() - mAgent.getLastSeenNestPosX()) < mAgent.getViewRadius() && getCaseState(mAgent.getLastSeenNestPosY(), mAgent.getLastSeenNestPosX()) != Nest){
		mAgent.setLastSeenNest(-1, -1);
	}

	//If the last seen fish pos is in view but not a fish anymore then reset the lastSeen fish
	if(abs(mAgent.getPosY() - mAgent.getLastSeenFishPosY()) < mAgent.getViewRadius() && abs(mAgent.getPosX() - mAgent.getLastSeenFishPosX()) < mAgent.getViewRadius() && getCaseState(mAgent.getLastSeenFishPosY(), mAgent.getLastSeenFishPosX()) != Fish){
		mAgent.setLastSeenFish(-1, -1);
	}

	mAgent.setNestInView(nestPosY,nestPosX);
	mAgent.setFishInView(fishPosY,fishPosX);
	mAgent.setGroundInView(groundPosY,groundPosX);
	mAgent.setWaterInView(waterPosY,waterPosX);
	mAgent.setMwettesInView(mwetteInView);

}

