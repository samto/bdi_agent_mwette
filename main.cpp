#include <stdio.h>
#include <stdlib.h>
#include <math.h> 
#include <iostream>
#include <string>
#include <time.h>
#include "Environnement.h"
#include "Mwette.h"

//--------------------------------------
//			TODO LIST
//--------------------------------------
//
// - Ajouter interaction Agent Homme -> Le sleep ne bloque pas les inputs
// - Utiliser "\033[1;31m" dans les cout pour colorier l'environnement et pouvoir sélectionner les mwettes en les changeant de couleurs
// 
//--------------------------------------

int main(int argc, char** argv){
	srand (time(NULL));

	bool displayDebug = true;

	Environnement envLac;
	envLac.init(displayDebug);

	Mwette joliMwette(rand()%envLac.getHeight(),rand()%envLac.getWidth(),displayDebug);
	Mwette gentilleMwette(rand()%envLac.getHeight(),rand()%envLac.getWidth(),displayDebug);
	Mwette mechanteMwette(rand()%envLac.getHeight(),rand()%envLac.getWidth(),displayDebug);
	Mwette mocheMwette(rand()%envLac.getHeight(),rand()%envLac.getWidth(),displayDebug);
	Mwette droleMwette(rand()%envLac.getHeight(),rand()%envLac.getWidth(),displayDebug);
	Mwette maladeMwette(rand()%envLac.getHeight(),rand()%envLac.getWidth(),displayDebug);

	envLac.addMwette(joliMwette);
	envLac.addMwette(gentilleMwette);
	envLac.addMwette(mechanteMwette);
	envLac.addMwette(mocheMwette);
	envLac.addMwette(droleMwette);
	envLac.addMwette(maladeMwette);
	envLac.gameLoop();

	//std::cout<<envLac.toString()<<std::endl;

	return 0;
}